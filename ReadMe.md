# BillingDemo

A demo billing service

The main concern when creating this service was maximising the amount of simultaneous user requests. To solve this
problem, the service uses an asynchronous request processing model. The web server part of a service stores user
transactions, and transaction processing happens in a dedicated looping worker.

An openapi3.0 description of a web server can be found at `src/docs/api.yaml`

## Launching a demo

This demo can be fired up by running a docker-compose.  
A `docker-compose up` command starts up a web server listening on `8080` port, a worker to process transactions, and a
prepared postgresql storage.  
(If it's a first run you might have to wait a couple of seconds for database schema to be applied and restart a
docker-compose)

## Demo interaction example

This set of command creates two accounts, adds a hundred units to one of them, transfers half of it to the other and
checks the result

```shell
curl --location --request PUT 'http://localhost:8080/accounts/acc1'

curl --location --request PUT 'http://localhost:8080/accounts/acc2'

curl --location --request POST 'http://localhost:8080/transactions' \
--header 'Content-Type: application/json' \
--data-raw '{"id": "tr1", "amount": "100", "to_": "acc1"}'

curl --location --request POST 'http://localhost:8080/transactions' \
--header 'Content-Type: application/json' \
--data-raw '{"id": "tr2", "amount": "50", "from_": "acc1", "to_": "acc2"}'

sleep 5s

curl --location --request GET 'http://localhost:8080/transactions/tr1'

curl --location --request GET 'http://localhost:8080/transactions/tr2'

curl --location --request GET 'http://localhost:8080/accounts/acc1'

curl --location --request GET 'http://localhost:8080/accounts/acc2'
```

Results:

```json
{
  "id": "tr1",
  "amount": 100.0,
  "from_": null,
  "to_": "acc1",
  "created_at": "2021-03-02T17:17:53.342792+00:00",
  "processed_at": "2021-03-02T17:17:54.818718+00:00",
  "result_code": "OK"
}
```

```json
{
  "id": "tr2",
  "amount": 50.0,
  "from_": "acc1",
  "to_": "acc2",
  "created_at": "2021-03-02T17:17:53.365686+00:00",
  "processed_at": "2021-03-02T17:17:54.834005+00:00",
  "result_code": "OK"
} 
```

```json
{
  "id": "acc1",
  "balance": 50.0,
  "created_at": "2021-03-02T17:17:53.271381+00:00",
  "modified_at": "2021-03-02T17:17:54.834005+00:00"
}
```

```json
{
  "id": "acc2",
  "balance": 50.0,
  "created_at": "2021-03-02T17:17:53.308580+00:00",
  "modified_at": "2021-03-02T17:17:54.834005+00:00"
}
```
