asyncpg==0.22
aiohttp==3.7
pydantic==1.7
uvloop==0.15
gunicorn==20.0
