from aiohttp import web

import app_config

_APP_KEY = 'config'


def init(app: web.Application):
    app[_APP_KEY] = app_config.get_environ_config()


def get(app: web.Application) -> app_config.Config:
    return app[_APP_KEY]
