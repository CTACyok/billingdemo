import dataclasses
import datetime
import decimal
import logging
import typing

from aiohttp import web
import asyncpg

from web import config

logger = logging.getLogger(__name__)

_APP_KEY = 'db_pool'

# TODO: extract repository interface and pg implementation


async def init(app: web.Application):
    cfg = config.get(app)
    app[_APP_KEY] = await asyncpg.create_pool(
        host=cfg.POSTGRES_HOST,
        port=cfg.POSTGRES_PORT,
        user=cfg.POSTGRES_USER,
        password=cfg.POSTGRES_PASSWORD,
        database=cfg.POSTGRES_DB,
    )
    app.on_shutdown.append(_on_shutdown)


async def _on_shutdown(app: web.Application):
    pool = _get_pool(app)
    await pool.close()


def _get_pool(app: web.Application) -> asyncpg.Pool:
    return app[_APP_KEY]


# no need for pydantic's validation here
@dataclasses.dataclass(frozen=True)
class Account:
    id: str
    balance: decimal.Decimal
    created_at: datetime.datetime
    modified_at: datetime.datetime


@dataclasses.dataclass(frozen=True)
class TransactionForm:
    id: str
    amount: decimal.Decimal
    from_: typing.Optional[str] = None
    to_: typing.Optional[str] = None


@dataclasses.dataclass(frozen=True)
class Transaction:
    id: str
    serial_id: int
    amount: decimal.Decimal
    created_at: datetime.datetime
    from_: typing.Optional[str] = None
    to_: typing.Optional[str] = None
    processed_at: typing.Optional[datetime.datetime] = None
    result_code: typing.Optional[str] = None


async def try_create_account(app: web.Application, acc_id: str) -> bool:
    sql_create = """
    insert into bd.accounts (id) values ($1)
        on conflict (id) do nothing
    returning id
    """
    record = await _get_pool(app).fetchrow(sql_create, acc_id)
    if record:
        logger.debug(f'Account created, id={acc_id}')
        return True
    logger.debug(f'Idempotency violation, creation skipped, id={acc_id}')
    return False


async def get_acc(app: web.Application, acc_id: str) -> typing.Optional[Account]:
    sql = "select * from bd.accounts where id=$1"
    record = await _get_pool(app).fetchrow(sql, acc_id)
    if record:
        return Account(**record)


async def try_create_transaction(app: web.Application, form: TransactionForm) -> typing.Optional[int]:
    sql_create = """
    insert into bd.transactions (id, amount, from_, to_)
        values ($1, $2, $3, $4)
        on conflict (id) do nothing
    returning id
    """
    async with _get_pool(app).acquire() as conn:  # type: asyncpg.Connection
        incremental_id = await _get_pool(app).fetchval(
            sql_create, form.id, form.amount, form.from_, form.to_,
        )
        if incremental_id:
            logger.info(f'Transaction created, id={form.id}')
            return incremental_id
    logger.info(f'Idempotency violation, creation skipped, id={form.id}')
    return None


async def get_transaction(app: web.Application, trans_id: str) -> typing.Optional[Transaction]:
    sql = "select * from bd.transactions where id=$1"
    record = await _get_pool(app).fetchrow(sql, trans_id)
    if record:
        return Transaction(**record)
