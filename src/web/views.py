import datetime
import decimal
import logging
import typing

from aiohttp import web
import pydantic

from web import db

logger = logging.getLogger(__name__)

routes = web.RouteTableDef()

# TODO: extract use_cases


@routes.get('/ping')
async def ping(request: web.Request):
    return web.Response()


@routes.put('/accounts/{id}')
async def try_create_account(request: web.Request):
    acc_id = request.match_info['id']
    is_new = await db.try_create_account(request.app, acc_id)
    return web.Response(status=is_new and 201 or 200)


@routes.get('/accounts/{id}')
async def get_account(request: web.Request):
    acc_id = request.match_info['id']
    db_account = await db.get_acc(request.app, acc_id)
    if not db_account:
        return web.json_response(
            text=ApiError(
                code='NOT_FOUND',
                message='Account not found',
                details={'id': acc_id},
            ).json(),
            status=404,
        )
    account = Account(**db_account.__dict__)
    return web.json_response(text=account.json())


@routes.post('/transactions')
async def try_create_transaction(request: web.Request):
    try:
        form = TransactionForm.parse_raw(await request.text())
        logger.debug('Got new transaction form %s', form)
    except pydantic.ValidationError as err:
        return web.json_response(
            text=ApiError(
                code='VALIDATION_ERROR',
                message='Form validation error',
                details={'errors': err.errors()},
            ).json(),
            status=400,
        )
    new_incremental_id = await db.try_create_transaction(
        app=request.app,
        form=db.TransactionForm(
            id=form.id,
            amount=form.amount,
            from_=form.from_,
            to_=form.to_,
        )
    )
    return web.Response(status=new_incremental_id is None and 200 or 201)


@routes.get('/transactions/{id}')
async def get_transaction(request: web.Request):
    trans_id = request.match_info['id']
    db_trans = await db.get_transaction(request.app, trans_id)
    if not db_trans:
        return web.json_response(
            text=ApiError(
                code='NOT_FOUND',
                message='Transaction not found',
                details={'id': trans_id},
            ).json(),
            status=404,
        )
    transactions = Transaction(**db_trans.__dict__)
    return web.json_response(text=transactions.json())


class Account(pydantic.BaseModel):
    id: str
    balance: decimal.Decimal
    created_at: datetime.datetime
    modified_at: datetime.datetime


class TransactionForm(pydantic.BaseModel):
    id: str
    amount: pydantic.condecimal(gt=decimal.Decimal(0))
    from_: typing.Optional[str]
    to_: typing.Optional[str]

    @pydantic.root_validator
    def validate_accounts(
            cls, values: typing.Dict[str, typing.Any],
    ):
        from_: typing.Optional[str] = values.get('from_')
        to_: typing.Optional[str] = values.get('to_')
        if to_ is None and from_ is None:
            raise ValueError('must have a from_ or to_ account')
        return values


class Transaction(pydantic.BaseModel):
    id: str
    amount: decimal.Decimal
    from_: typing.Optional[str] = None
    to_: typing.Optional[str] = None
    created_at: datetime.datetime
    processed_at: typing.Optional[datetime.datetime] = None
    result_code: typing.Optional[str] = None


class ApiError(pydantic.BaseModel):
    code: str
    message: str
    details: typing.Optional[typing.Dict[str, typing.Any]] = None


def init(app: web.Application):
    app.add_routes(routes)
