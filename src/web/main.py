import logging

from aiohttp import web

from web import config
from web import db
from web import views


async def init_app():
    logging.basicConfig(level=logging.DEBUG)

    app = web.Application()

    config.init(app)
    await db.init(app)
    views.init(app)

    return app


# gunicorn web.main:init_app --bind localhost:8080 --worker-class aiohttp.GunicornUVLoopWebWorker

if __name__ == '__main__':
    web.run_app(init_app())
