create schema if not exists bd;

create table if not exists bd.accounts
(
    id          text primary key,
    balance     numeric     not null default 0,
    created_at  timestamptz not null default now(),
    modified_at timestamptz not null default now()
);

create table if not exists bd.transactions
(
    id           text primary key,
    serial_id    bigserial   not null,
    amount       numeric     not null,
    from_        text references bd.accounts (id) on delete restrict,
    to_          text references bd.accounts (id) on delete restrict,
    created_at   timestamptz not null default now(),
    processed_at timestamptz,
    result_code  text
);

create index if not exists idx_unprocessed_transactions on bd.transactions (serial_id) where processed_at is null;
