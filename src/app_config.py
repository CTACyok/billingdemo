import os

import pydantic

_APP_KEY = 'config'


class Config(pydantic.BaseModel):
    POSTGRES_PASSWORD: str
    POSTGRES_USER: str
    POSTGRES_DB: str
    POSTGRES_HOST: str = 'postgres'
    POSTGRES_PORT: int = 5432


def get_environ_config() -> Config:
    return Config(**os.environ)
