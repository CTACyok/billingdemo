import asyncio
import dataclasses
import decimal
import logging
import typing

import asyncpg

import app_config

logger = logging.getLogger(__name__)


@dataclasses.dataclass(frozen=True)
class Account:
    id: str
    balance: decimal.Decimal


@dataclasses.dataclass(frozen=True)
class Transaction:
    id: str
    amount: decimal.Decimal
    from_: typing.Optional[Account]
    to_: typing.Optional[Account]

    def process(self) -> 'Transaction':
        updated_from = None
        if self.from_:
            new_from_balance = self.from_.balance - self.amount
            if new_from_balance < 0:
                raise TransactionProcessingError('INSUFFICIENT_BALANCE')
            updated_from = Account(id=self.from_.id, balance=new_from_balance)
        updated_to = None
        if self.to_:
            new_to_balance = self.to_.balance + self.amount
            updated_to = Account(id=self.to_.id, balance=new_to_balance)
        return Transaction(
            id=self.id,
            amount=self.amount,
            from_=updated_from,
            to_=updated_to,
        )


class TransactionProcessingError(Exception):
    def __init__(self, code: str):
        self.code = code


GET_NEXT_TRANSACTION = """
select id, amount, from_, to_
from bd.transactions
where processed_at is null
order by serial_id
limit 1
for no key update
"""
GET_ACCOUNT = """
select id, balance
from bd.accounts
where id = $1
for no key update
"""
STORE_TRANSACTION_RESULT = """
update bd.transactions
set processed_at = now(),
    result_code = $2
where id = $1
"""
UPDATE_ACCOUNT_BALANCE = """
update bd.accounts
set modified_at = now(),
    balance = $2
where id = $1
"""


async def _fetch_next_transaction(connection: asyncpg.Connection) -> typing.Optional[Transaction]:
    transaction_row = await connection.fetchrow(GET_NEXT_TRANSACTION)
    if not transaction_row:
        return None
    from_ = None
    if transaction_row['from_']:
        from_row = await connection.fetchrow(GET_ACCOUNT, transaction_row['from_'])
        from_ = Account(
            id=from_row['id'],
            balance=from_row['balance'],
        )
    to_ = None
    if transaction_row['to_']:
        to_row = await connection.fetchrow(GET_ACCOUNT, transaction_row['to_'])
        to_ = Account(
            id=to_row['id'],
            balance=to_row['balance'],
        )
    return Transaction(
        id=transaction_row['id'],
        amount=transaction_row['amount'],
        from_=from_,
        to_=to_,
    )


async def process_queue(connection: asyncpg.Connection, **kwargs):
    logger.info('Processing queue')
    while True:
        async with connection.transaction():
            new_transaction = await _fetch_next_transaction(connection)
            if not new_transaction:
                logger.info('No transactions to process')
                return
            logger.info(f'Processing transaction {new_transaction.id}')
            logger.debug('Current state: %s', new_transaction)
            try:
                processed_transaction = new_transaction.process()
            except TransactionProcessingError as err:
                logger.info(f'Transaction error {err}')
                await connection.execute(
                    STORE_TRANSACTION_RESULT, new_transaction.id, err.code,
                )
            else:
                logger.info(f'Transaction processed {processed_transaction.id}')
                logger.debug('New state: %s', processed_transaction)
                await connection.execute(
                    STORE_TRANSACTION_RESULT, processed_transaction.id, 'OK',
                )
                from_ = processed_transaction.from_
                if from_:
                    await connection.execute(
                        UPDATE_ACCOUNT_BALANCE, from_.id, from_.balance,
                    )
                to_ = processed_transaction.to_
                if to_:
                    await connection.execute(
                        UPDATE_ACCOUNT_BALANCE, to_.id, to_.balance,
                    )


async def main():
    cfg = app_config.get_environ_config()
    connection: asyncpg.Connection = await asyncpg.connect(
        host=cfg.POSTGRES_HOST,
        port=cfg.POSTGRES_PORT,
        user=cfg.POSTGRES_USER,
        password=cfg.POSTGRES_PASSWORD,
        database=cfg.POSTGRES_DB,
    )
    while True:
        await process_queue(connection)
        await asyncio.sleep(5)


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    asyncio.run(main())
